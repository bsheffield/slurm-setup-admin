# SLURM-Admin-Setup

SLURM Administrator Install/Configuration Instructions

Set up SLURM without user authentication using Munge(for simplicity) on an existing Red Hat
Enterprise Linux 8 machine. These instructions are intended to support PyTorch training job submissions for
clients.

## Install SLURM with MariaDB

1. Update the system:
```
sudo dnf update -y
```
2. Install required dependencies:
```
sudo dnf install -y epel-release
sudo dnf install -y mariadb-server mariadb-devel openssl openssl-devel pam-devel numactl
numactl-devel hwloc hwloc-devel lua lua-devel readline-devel rrdtool-devel ncurses-devel
man2html libibmad libibumad perl-ExtUtils-MakeMaker
```
3. Enable and start the MariaDB service:
```
sudo systemctl enable --now mariadb
```
4. Secure the MariaDB installation and create the SLURM database and user:
```
sudo mysql_secure_installation
sudo mysql -u root -p
```
Inside MariaDB prompt, execute the following commands:
```
CREATE DATABASE slurm_acct_db;
CREATE USER 'slurm'@'localhost' IDENTIFIED BY 'your_password';
GRANT ALL PRIVILEGES ON slurm_acct_db.* TO 'slurm'@'localhost';
FLUSH PRIVILEGES;
exit;
```
5. Download and install SLURM:
```
wget https://download.schedmd.com/slurm/slurm-20.11.8.tar.bz2
tar -xjf slurm-20.11.8.tar.bz2
cd slurm-20.11.8
./configure
make
sudo make install
```
6. Create and configure SLURM configuration files:
```
sudo mkdir /etc/slurm
sudo cp etc/slurm.conf.example /etc/slurm/slurm.conf
sudo cp etc/slurmdbd.conf.example /etc/slurm/slurmdbd.conf
```
Update `/etc/slurm/slurm.conf` and `/etc/slurm/slurmdbd.conf` with your cluster-specific
settings (e.g., ControlMachine, NodeName, PartitionName, etc.). Ensure that the MariaDB
database and user details are correctly set in the `slurmdbd.conf` file.
7. Create the required directories and set permissions:
```
sudo install -d -m 0700 -o slurm /var/spool/slurmd
sudo install -d -m 0755 -o slurm /var/run/slurm
sudo install -d -m 0755 -o slurm /var/log/slurm
```
8. Enable and start SLURM services:
```
sudo systemctl enable slurmctld slurmd slurmdbd
sudo systemctl start slurmctld slurmd slurmdbd
```
9. Install required Python packages and PyTorch:
```
sudo dnf install -y python3 python3-pip
sudo pip3 install torch torchvision
```

## Connect Additional GPU Nodes

To connect additional nodes with GPUs to the SLURM cluster, follow these steps:
1. **Install necessary software**: Install SLURM and the required software on the new node(s)
following the same process as the existing nodes. This includes SLURM, MUNGE (if used for
authentication), and any required GPU drivers and libraries (e.g., CUDA, cuDNN).
2. **Configure the new node**: Make sure the new node's hostname is resolvable and has
proper network connectivity with the existing SLURM nodes. Ensure that the SLURM and
MUNGE services are running and configured correctly, with the same versions and
configurations as the existing nodes.
3. **Update the SLURM configuration**: Modify the `slurm.conf` file on the SLURM controller
node to include the new node(s) and their resources:
a. Open the `slurm.conf` file with a text editor:
```bash
sudo nano /etc/slurm/slurm.conf
```
b. Add the new node to the `NodeName` parameter in the `slurm.conf` file, specifying the
resources like GPUs, CPU cores, and memory:
```
NodeName=new_node_name Gres=gpu:2 CPUs=16 RealMemory=64000
```
Replace `new_node_name` with the actual hostname of the new node and adjust the
resources according to the new node's hardware specifications.
c. Update the `PartitionName` parameter to include the new node:
```
PartitionName=gpu Nodes=existing_node1,existing_node2,new_node_name Default=NO
MaxTime=INFINITE State=UP
```
Replace `existing_node1`, `existing_node2`, and `new_node_name` with the actual
hostnames of the existing and new nodes.
d. Save and close the `slurm.conf` file.
4. **Distribute the updated configuration**: Copy the updated `slurm.conf` file to all nodes in the
cluster, including the new node(s):
```bash
scp /etc/slurm/slurm.conf user@new_node:/etc/slurm/slurm.conf
```
Replace `user` with your username and `new_node` with the hostname of the new node. You
may need to use `sudo` to copy the file to the correct location on the new node.
5. **Restart SLURM services**: Restart the SLURM services on all nodes, including the new
node(s), for the changes to take effect:
```bash
sudo systemctl restart slurmctld slurmd
```
6. **Verify the new node**: Check if the new node is visible and available in the SLURM cluster
using the `sinfo` command:
```bash
sinfo
```
You should see the new node listed with the appropriate resources and partition.


## Test Job Submission

We will use SubmitIt for testing a job submission. SubmitIt is a Python library developed by
Facebook AI Research (FAIR) that allows users to easily submit jobs to a SLURM cluster.
Here's an example of using SubmitIt to submit a PyTorch training job to the SLURM cluster:
1. Install SubmitIt library:
```
pip3 install submitit
```
2. Run the `submit_job.py` script to submit the PyTorch training job to the SLURM cluster:
```
python3 submit_job.py
```
The script will submit the job and print the job ID. You can use this ID to monitor the job's
progress using SLURM commands like `squeue`, `scontrol`, and `sacct`. The job's output, along
with SubmitIt logs, will be saved in the `submitit_logs` folder.

## Tools to Help Configure Resources for Nodes

To gather information about your system's CPUs, memory, and GPU resources, you can use the
following CLI commands:
1. CPU cores and model:
```bash
lscpu
```
This command will display information about your system's CPU(s), including the number of
cores, threads, and CPU model.
2. Total memory:
```bash
free -h
```
This command will display information about your system's memory usage. The total memory
can be found in the 'Mem' row under the 'total' column.
3. GPU information:
If you have NVIDIA GPUs, you can use the `nvidia-smi` command:
```bash
nvidia-smi
```
This command will display information about your AMD GPUs, including GPU model, memory,
and utilization.
4. Additional system information:
```bash
lshw
```
This command will display a comprehensive report about your system's hardware, including
CPU, memory, storage devices, and GPUs.
You can use the information gathered from these commands to configure the `NodeName`
parameters in the `slurm.conf` file. For example, suppose you have a system with 16 CPU
cores, 64GB of memory, and 2 NVIDIA GPUs. In that case, you can set the `NodeName`
parameters like this:
```
NodeName=node1 Gres=gpu:2 CPUs=16 RealMemory=64000
```
Make sure to adjust the values according to your system's hardware specifications.

## Troubleshooting Submitted Jobs

The error message you're seeing in `squeue` indicates that the required nodes for your job are
in a DOWN, DRAINED, or reserved state for jobs in higher priority partitions.
To investigate and resolve this issue, you can follow these steps:
1. Check the status of the nodes using `sinfo`. This command will display the status of all nodes
and partitions:
```bash
sinfo
```
Look for the 'gpu' partition and check the state of the nodes within it. If the nodes are in a
DOWN or DRAINED state, you'll need to bring them back to an active state.
2. If the nodes are in a DOWN state, use the following command to bring them back up:
```bash
sudo scontrol update NodeName=<node_name> State=RESUME
```
Replace `<node_name>` with the name of the node that is down. Repeat this for all nodes that
are in a DOWN state.
3. If the nodes are in a DRAINED state, it means they were either drained manually or due to an
issue. To bring the nodes back to an active state, use the following command:
```bash
sudo scontrol update NodeName=<node_name> State=UNDRAIN
```
Replace `<node_name>` with the name of the node that is drained. Repeat this for all nodes
that are in a DRAINED state.
4. If the nodes are reserved for jobs in higher priority partitions, you can either wait for those
jobs to complete, or if you have administrative privileges, you can adjust job priorities using
`scontrol`. For example, to increase the priority of your job, use:
```bash
sudo scontrol update JobId=<job_id> Priority=<new_priority>
```
Replace `<job_id>` with the actual job ID and `<new_priority>` with the desired priority value.
After resolving the issue, your job should be able to run on the 'gpu' partition without any
problem.

