import os
import submitit
from submitit.core.utils import CommandFunction

class CondaExecutor(submitit.AutoExecutor):
    def __init__(self, conda_env=None, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.conda_env = conda_env
        if conda_env is not None:
            self.conda_setup(conda_env)

    def conda_setup(self, env_name):
        yml_file = "environment.yml"
        if os.path.exists(f"{os.getenv('HOME')}/miniconda3/envs/{env_name}"):
            print(f"Conda environment {env_name} exists.")
        else:
            print(f"Creating conda environment {env_name} from {yml_file}.")
            os.system(f"conda env create -f {yml_file}")

    def get_init_command(self, *args, **kwargs):
        command = super().get_init_command(*args, **kwargs)
        if self.conda_env is not None:
            command = f"source activate {self.conda_env} && " + command
        return command

def main():
    executor = CondaExecutor(folder="submitit_logs", conda_env="myenv")
    executor.update_parameters(
        name="pytorch-lightning-training",
        partition="gpu",
        time="01:00:00",
        gpus_per_node=1,
        ntasks_per_node=1,
        mem_gb=32,
    )

    command = f"python train.py"
    executor.submit(CommandFunction(command))

if __name__ == "__main__":
    main()
